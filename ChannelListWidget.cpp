#include "ChannelListWidget.h"
#include <QMouseEvent>
#include <Windows.h>
#include <QBoxLayout>
#include <QFile>
#include <QApplication>


ChannelTree::ChannelTree(ChannelListWidget * widget, QWidget *parent)
	: QTreeWidget(parent)
{
	m_parent = widget;

	setHeaderHidden(true);
	setExpandsOnDoubleClick(false);

	m_delegate = new MyItemDelegate;
	//setItemDelegate(m_delegate);

	QFile styleSheet("plugins\\qtTsOverlay\\tree.styl");
	styleSheet.open(QIODevice::ReadOnly);
	setStyleSheet(styleSheet.readAll());
	styleSheet.close();

	setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	setAttribute(Qt::WA_ShowWithoutActivating);
	setFocusPolicy(Qt::NoFocus);
}

ChannelTree::~ChannelTree()
{
	delete m_delegate;
}

void ChannelTree::mousePressEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
	{
		m_oldPosition = event->globalPos();
		doMove = true;
	}
	else if (event->button() == Qt::RightButton)
		m_parent->hide();

	QTreeWidget::mousePressEvent(event);
}

void ChannelTree::mouseReleaseEvent(QMouseEvent * event)
{
	doMove = false;

	QTreeWidget::mouseReleaseEvent(event);
}

void ChannelTree::mouseMoveEvent(QMouseEvent * event)
{
	if (doMove)
	{
		m_parent->move(m_parent->pos() + (event->globalPos() - m_oldPosition));
		m_oldPosition = event->globalPos();
	}

	QTreeWidget::mouseMoveEvent(event);
}


///////////////////////////////////////////////////////////////////////////////////////

Background::Background(ChannelListWidget * widget, QWidget *parent)
	: QLabel(parent)
{
	m_parent = widget;

	QFile styleSheet("plugins\\qtTsOverlay\\background.styl");
	styleSheet.open(QIODevice::ReadOnly);
	setStyleSheet(styleSheet.readAll());
	styleSheet.close();
}

Background::~Background()
{

}

void Background::mousePressEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
	{
		m_oldPosition = event->globalPos();
		doMove = true;
	}
	else if (event->button() == Qt::RightButton)
		m_parent->hide();

	QLabel::mousePressEvent(event);
}

void Background::mouseReleaseEvent(QMouseEvent * event)
{
	doMove = false;

	QLabel::mouseReleaseEvent(event);
}

void Background::mouseMoveEvent(QMouseEvent * event)
{
	if (doMove)
	{
		m_parent->move(m_parent->pos() + (event->globalPos() - m_oldPosition));
		m_oldPosition = event->globalPos();
	}

	QLabel::mouseMoveEvent(event);
}


///////////////////////////////////////////////////////////////////////////////////////

ChannelListWidget::ChannelListWidget(QWidget *parent)
	: QWidget(parent)
{
	m_state = full;

	m_minIcon.addFile("plugins\\qtTsOverlay\\min.png");
	m_minIcon.addFile("plugins\\qtTsOverlay\\min.png", QSize(), QIcon::Selected);
	m_maxIcon.addFile("plugins\\qtTsOverlay\\max.png");
	m_maxIcon.addFile("plugins\\qtTsOverlay\\max.png", QSize(), QIcon::Selected);

	setAttribute(Qt::WA_TranslucentBackground);
	setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::SplashScreen);
	move(320, 200);

	m_background = new Background(this, this);

	QVBoxLayout* layout = new QVBoxLayout(this);
	layout->setMargin(0);
	layout->setSpacing(0);

	m_button = new QPushButton(this);
	m_button->setIcon(m_minIcon);
	m_button->setIconSize(QSize(8,8));
	connect(m_button, &QPushButton::pressed, this, &ChannelListWidget::buttonPressed);
	QFile style("plugins\\qtTsOverlay\\button.styl");
	style.open(QIODevice::ReadOnly);
	m_button->setStyleSheet(style.readAll());
	style.close();
	layout->addWidget(m_button, 0, Qt::AlignTop | Qt::AlignRight);

	m_child = new ChannelTree(this);
	layout->addWidget(m_child);

	m_grip = new QSizeGrip(this);
	layout->addWidget(m_grip, 0, Qt::AlignBottom | Qt::AlignRight);

	show();
}

ChannelListWidget::~ChannelListWidget()
{
	delete m_child;
	delete m_grip;
	delete m_background;
	delete m_button;
}

QTreeWidget * ChannelListWidget::getTree()
{
	return (QTreeWidget*)m_child;
}

void ChannelListWidget::buttonPressed()
{
	if (m_state == full)
	{
		m_state = minimum;
		m_button->setIcon(m_maxIcon);
	}
	else
	{
		m_state = full;
		m_button->setIcon(m_minIcon);
	}

	emit treeStateChanged(m_state);
}

void ChannelListWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);

	m_child->resize(m_child->size() - (event->oldSize() - event->size()));
	m_background->resize(size());
}
