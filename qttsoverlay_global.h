#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(QTTSOVERLAY_LIB)
#  define QTTSOVERLAY_EXPORT Q_DECL_EXPORT
# else
#  define QTTSOVERLAY_EXPORT Q_DECL_IMPORT
# endif
#else
# define QTTSOVERLAY_EXPORT
#endif
