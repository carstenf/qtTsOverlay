#pragma once
#include <QWidget>
#include <QPoint>
#include <QTreeWidget>
#include <QSizeGrip>
#include <QLabel>
#include <QPushButton>
#include <QIcon>
#include <QItemDelegate>
#include <QPainter>

enum TreeState {
	full,
	minimum
};

class ChannelListWidget;

class MyItemDelegate : public QItemDelegate
{
public:
	void paint(QPainter * painter, const QStyleOptionViewItem & oStyleOption, const QModelIndex & index) const
	{
		QStyleOptionViewItem oStyleOpt = oStyleOption;

		if (oStyleOpt.state & QStyle::State_Selected) {
			oStyleOpt.state ^= QStyle::State_Selected;
		}

		/*if (oStyleOpt.state & QStyle::State_HasFocus) {
			oStyleOpt.state ^= QStyle::State_HasFocus;
		}*/
		
		// Paint
		QItemDelegate::paint(painter, oStyleOpt, index);
	}
};

class ChannelTree : public QTreeWidget
{
	Q_OBJECT

public:
	explicit ChannelTree(ChannelListWidget * widget, QWidget *parent = NULL);
	~ChannelTree();

protected:
	virtual void mousePressEvent(QMouseEvent * event) Q_DECL_OVERRIDE;
	virtual void mouseReleaseEvent(QMouseEvent * event) Q_DECL_OVERRIDE;
	virtual void mouseMoveEvent(QMouseEvent * event) Q_DECL_OVERRIDE;

private:
	bool doMove = false;
	QPoint m_oldPosition;
	QWidget *m_parent;
	MyItemDelegate* m_delegate;

};

class Background : public QLabel
{
	Q_OBJECT

public:
	explicit Background(ChannelListWidget * widget, QWidget *parent = NULL);
	~Background();

protected:
	virtual void mousePressEvent(QMouseEvent * event) Q_DECL_OVERRIDE;
	virtual void mouseReleaseEvent(QMouseEvent * event) Q_DECL_OVERRIDE;
	virtual void mouseMoveEvent(QMouseEvent * event) Q_DECL_OVERRIDE;

private:
	bool doMove = false;
	QPoint m_oldPosition;
	QWidget *m_parent;

};

class ChannelListWidget : public QWidget
{
	Q_OBJECT

public:
	explicit ChannelListWidget(QWidget *parent = NULL);
	~ChannelListWidget();

private:
	TreeState m_state;
	ChannelTree *m_child;
	QSizeGrip *m_grip;
	Background *m_background;
	QPushButton* m_button;
	QIcon m_minIcon;
	QIcon m_maxIcon;

private slots:
	void buttonPressed();

protected:
	virtual void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

public:
	QTreeWidget* getTree();

signals:
	void treeStateChanged(TreeState state);
};
