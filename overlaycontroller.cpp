#include "overlaycontroller.h"
#include <QDesktopWidget>
#include <QStringList>
#include "plugin.h"
#include <qdebug.h>
#include "teamspeak\public_errors.h"
#include <QRegExp>
#include <QFile>

//#define DEBUGWINDOW

enum nodeType {
	channel,
	client,
	spacer
};

Q_DECLARE_METATYPE(nodeType);

OverlayController::OverlayController(const struct TS3Functions funcs, quint64 serverConnectionHandlerID) : QObject(), m_ts3(funcs)
{
	QDesktopWidget desktop;

	m_muted = false;
	m_treeState = full;

	m_channelIcon.addFile("plugins\\qtTsOverlay\\channel.png");
	m_channelIcon.addFile("plugins\\qtTsOverlay\\channel.png", QSize(), QIcon::Selected);
	m_clientIcon.normal.addFile("plugins\\qtTsOverlay\\client.png");
	m_clientIcon.normal.addFile("plugins\\qtTsOverlay\\client.png", QSize(), QIcon::Selected);
	m_clientIcon.microMute.addFile("plugins\\qtTsOverlay\\microMute.png");
	m_clientIcon.microMute.addFile("plugins\\qtTsOverlay\\microMute.png", QSize(), QIcon::Selected);
	m_clientIcon.speakerMute.addFile("plugins\\qtTsOverlay\\speakerMute.png");
	m_clientIcon.speakerMute.addFile("plugins\\qtTsOverlay\\speakerMute.png", QSize(), QIcon::Selected);
	m_clientIcon.doubleMute.addFile("plugins\\qtTsOverlay\\doubleMute.png");
	m_clientIcon.doubleMute.addFile("plugins\\qtTsOverlay\\doubleMute.png", QSize(), QIcon::Selected);

	QFile styleSheet("plugins\\qtTsOverlay\\label.styl");
	styleSheet.open(QIODevice::ReadOnly);
	m_stylesheet = styleSheet.readAll();
	styleSheet.close();

	m_screenHeight = desktop.screenGeometry().height();
	m_screenWidth = desktop.screenGeometry().width();

	m_speakerOffset = 0;

#ifdef DEBUGWINDOW
	m_debugWindow = new QLabel;
	m_debugWindow->setGeometry(320, 200, 750, 500);
	m_debugWindow->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	m_debugWindow->show();
#else
	m_debugWindow = NULL;
#endif

	m_SCHID = serverConnectionHandlerID;

	m_tree = new ChannelListWidget;
	connect(m_tree->getTree(), &QTreeWidget::itemDoubleClicked, this, &OverlayController::treeItemClicked);
	connect(m_tree, &ChannelListWidget::treeStateChanged, this, &OverlayController::changeTreeState);

	updateChannelList();
	updateClientList();
}

OverlayController::~OverlayController()
{
	for (auto &it : m_speakers)
		delete it;
	m_speakers.clear();

	for (auto &it : m_msgLines)
		delete it;
	m_msgLines.clear();

	m_channelList.clear();

	while (!m_clientList.isEmpty())
		delete m_clientList.takeFirst();

	if (m_debugWindow != NULL)
		delete m_debugWindow;
	delete m_tree;
}

void OverlayController::deleteChatLine(QWidget * line, QTimer *timer)
{
	int index = m_msgLines.indexOf(line);
	if (index != -1)
	{
		delete m_msgLines.at(index);
		m_msgLines.remove(index);
	}

	if (timer)
		delete timer;
}

void OverlayController::treeItemClicked(QTreeWidgetItem * item, int column)
{
	int type = item->data(2, Qt::UserRole).toInt();

	if (type == channel)
	{
		anyID clientID;
		m_ts3.getClientID(m_SCHID, &clientID);

		quint64 id = item->data(1, Qt::UserRole).value<quint64>();
		m_ts3.requestClientMove(m_SCHID, clientID, id, "", NULL);
	}
	else if (type == client)
	{
		anyID ownID;
		m_ts3.getClientID(m_SCHID, &ownID);
		anyID id = item->data(1, Qt::UserRole).value<anyID>();

		debugPrint(QString("ownID: %1 id: %2").arg(ownID).arg(id));

		if (id == ownID)
		{
			int value[3];
			if (m_ts3.getClientSelfVariableAsInt(m_SCHID, CLIENT_INPUT_MUTED, &value[0]) != ERROR_ok)
				value[0] = -1;
			if (m_ts3.getClientSelfVariableAsInt(m_SCHID, CLIENT_OUTPUT_MUTED, &value[1]) != ERROR_ok)
				value[1] = -1;
			if (m_ts3.getClientSelfVariableAsInt(m_SCHID, CLIENT_INPUT_DEACTIVATED, &value[2]) != ERROR_ok)
				value[2] = -1;

			// we have all information, nothing is lost
			if (value[0] != -1 && value[1] != -1 && value[3] != -1)
			{
				// something was disabled => enable everything
				if (value[2])
				{
					m_ts3.setClientSelfVariableAsInt(m_SCHID, CLIENT_INPUT_DEACTIVATED, 0);
				}
				else if (value[0] || value[1])
				{
					m_ts3.setClientSelfVariableAsInt(m_SCHID, CLIENT_INPUT_MUTED, 0);
					m_ts3.setClientSelfVariableAsInt(m_SCHID, CLIENT_OUTPUT_MUTED, 0);
				}
				// nothing was disabled => disable everything
				else
				{
					m_ts3.setClientSelfVariableAsInt(m_SCHID, CLIENT_INPUT_MUTED, 1);
					m_ts3.setClientSelfVariableAsInt(m_SCHID, CLIENT_OUTPUT_MUTED, 1);
				}
			}
		}
		else
			m_ts3.requestClientPoke(m_SCHID, id, "", NULL);
	}
}

void OverlayController::changeTreeState(TreeState state)
{
	m_treeState = state;
	updateChannelList();
	updateClientList();
}

void OverlayController::addChatLine(QString message)
{
	// generate new chatline
	QWidget* w = new QWidget;
	QLabel* newChatLine = new QLabel(w);

	w->setAttribute(Qt::WA_TranslucentBackground);
	w->setAttribute(Qt::WA_ShowWithoutActivating);
	w->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::WindowTransparentForInput | Qt::SplashScreen);

	newChatLine->setText(message);
	newChatLine->setStyleSheet(m_stylesheet);
	newChatLine->adjustSize();

	newChatLine->setAttribute(Qt::WA_ShowWithoutActivating);
	newChatLine->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowTransparentForInput);
	newChatLine->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);

	w->adjustSize();
	w->setGeometry(BORDEROFFSET, BORDEROFFSET, w->size().width(), w->size().height() + SPACING);

	newChatLine->show();
	w->show();

	// move old messages down
	for (auto &it : m_msgLines)
		it->setGeometry(it->geometry().x(), it->geometry().y() + w->size().height(), it->geometry().width(), it->geometry().height());

	// inseart new chat line
	m_msgLines.push_front(w);

	// limit the number of lines
	if (m_msgLines.size() > MAXLINES)
		deleteChatLine(m_msgLines.last());

	// hide after time
	QTimer* timer = new QTimer(this);
	timer->setSingleShot(true);
	timer->setInterval(TIMEOUT);
	connect(timer, &QTimer::timeout, [=]() {deleteChatLine(w, timer);});
	timer->start();
}

void OverlayController::addSpeaker(QString name)
{
	if (m_muted)
		return;

	int labelWidth = 0;
	QWidget* w = new QWidget;
	QLabel* newSpeaker = new QLabel(w);

	w->setAttribute(Qt::WA_TranslucentBackground);
	w->setAttribute(Qt::WA_ShowWithoutActivating);
	w->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::WindowTransparentForInput | Qt::SplashScreen);
	w->setObjectName(name);

	newSpeaker->setText(name);
	newSpeaker->setStyleSheet(m_stylesheet);
	newSpeaker->adjustSize();
	
	newSpeaker->setAttribute(Qt::WA_ShowWithoutActivating);
	newSpeaker->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowTransparentForInput);
	newSpeaker->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);

	w->adjustSize();
	w->setGeometry(BORDEROFFSET + m_speakerOffset, m_screenHeight - BORDEROFFSET - w->size().height(), w->size().width() + SPACING, w->size().height());
	labelWidth = w->geometry().width();

	newSpeaker->show();
	w->show();

	m_speakerOffset += labelWidth;

	m_speakers.push_back(w);
}

void OverlayController::removeSpeaker(QString name)
{
	int index = -1;
	int labelWidth = 0;

	// move all speaker to the left side and find the one to delete
	for (int i = 0; i < m_speakers.size(); i++)
	{
		QWidget* it = m_speakers.at(i);

		it->setGeometry(it->geometry().x() - labelWidth, it->geometry().y(), it->geometry().width(), it->geometry().height());

		if (it->objectName() == name)
		{
			labelWidth = it->geometry().width();
			m_speakerOffset -= labelWidth;
			index = i;
			delete it;
		}
	}

	// remove the deleted label from the list
	if (index != -1)
		m_speakers.remove(index);
}

void OverlayController::debugPrint(QString text)
{
	if (m_debugWindow != NULL)
		m_debugWindow->setText(text + "\n" + m_debugWindow->text());
}

void OverlayController::reset()
{
	for (auto &it : m_speakers)
		delete it;
	m_speakers.clear();

	for (auto &it : m_msgLines)
		delete it;
	m_msgLines.clear();

	m_speakerOffset = 0;
}

void OverlayController::updateChannelList()
{
	// get list of all channelIDs
	uint64* channelIDList;
	if (m_ts3.getChannelList(m_SCHID, &channelIDList) != ERROR_ok)
		return;

	// remove old stuff
	while (int nb = m_tree->getTree()->topLevelItemCount())
		delete m_tree->getTree()->takeTopLevelItem(nb - 1);
	m_clientList.clear();
	m_channelList.clear();

	// get a list of all channels containing name, id, parent,..
	int i(0);
	while (channelIDList[i] != NULL)
	{
		ChannelInfo tmp;
		tmp.id = channelIDList[i];
		tmp.name = channelID2Name(m_SCHID, channelIDList[i]);
		m_ts3.getChannelVariableAsInt(m_SCHID, tmp.id, CHANNEL_ORDER, &tmp.order);
		m_ts3.getParentChannelOfChannel(m_SCHID, channelIDList[i], &tmp.parent);
		tmp.entry = new QTreeWidgetItem;
		tmp.entry->setData(0, Qt::DisplayRole, tmp.name);
		tmp.entry->setData(1, Qt::UserRole, tmp.id);
		if (tmp.name.startsWith('[') && tmp.name.contains("spacer"))
		{
			tmp.entry->setData(2, Qt::UserRole, spacer);
			tmp.name = tmp.name.remove(QRegExp("\\[.*spacer.*\\]"));
			tmp.entry->setData(0, Qt::DisplayRole, tmp.name);
		}
		else
		{
			tmp.entry->setData(2, Qt::UserRole, channel);
			tmp.entry->setIcon(0, m_channelIcon);
		}

		m_channelList.push_back(tmp);
		i++;
	}

	if (m_treeState == full)
	{

		// for every channel
		for (auto &parent : m_channelList)
		{
			// collect all childs
			QVector<ChannelInfo> childs;
			for (auto &child : m_channelList)
				if (child.parent == parent.id)
					childs.push_back(child);

			// add childs in correct order
			int prevID(0);
			while (!childs.isEmpty())
			{
				int i;
				for (i = 0; i < childs.size(); i++)
					if (childs[i].order == prevID)
						break;

				prevID = childs[i].id;
				parent.entry->addChild(childs.takeAt(i).entry);
			}
		}

		// get all toplvl channels
		QVector<ChannelInfo> topLvlList;
		for (auto &it : m_channelList)
			if (it.parent == 0)
				topLvlList.push_back(it);

		// add toplvl to tree in correct order
		int prevID = 0;
		while (!topLvlList.isEmpty())
		{
			int i;
			for (i = 0; i < topLvlList.size(); i++)
				if (topLvlList[i].order == prevID)
					break;

			prevID = topLvlList[i].id;
			m_tree->getTree()->addTopLevelItem(topLvlList.takeAt(i).entry);
		}
	}
	else if (m_treeState == minimum)
	{
		//TODO: different order
		uint64 channel = getCurrentChannel(m_SCHID);
		QTreeWidgetItem* item = NULL;

		for (auto &it : m_channelList)
			if (it.id == channel)
				item = it.entry;

		if (item == NULL)
		{
			debugPrint("i was here");
			m_ts3.freeMemory(channelIDList);
			return;
		}
		m_tree->getTree()->addTopLevelItem(item);

	}

	m_tree->getTree()->expandAll();
	m_ts3.freeMemory(channelIDList);
}

void OverlayController::updateClientList()
{
	if (m_treeState == minimum)
		updateChannelList();

	//remove all clients
	while (!m_clientList.isEmpty())
		delete m_clientList.takeFirst();

	//add all clients
	for (auto& it : m_channelList)
	{
		anyID* clientIDList;
		if (m_ts3.getChannelClientList(m_SCHID, it.id, &clientIDList) != ERROR_ok)
			continue;

		int i(0);
		while (clientIDList[i] != NULL)
		{
			QTreeWidgetItem* tmp = new QTreeWidgetItem;
			tmp->setData(0, Qt::DisplayRole, clientID2Name(m_SCHID, clientIDList[i]));
			int value[3];
			if (m_ts3.getClientVariableAsInt(m_SCHID, clientIDList[i], CLIENT_INPUT_MUTED, &value[0]) != ERROR_ok)
				value[0] = -1;
			if (m_ts3.getClientVariableAsInt(m_SCHID, clientIDList[i], CLIENT_OUTPUT_MUTED, &value[1]) != ERROR_ok)
				value[1] = -1;
			if (m_ts3.getClientVariableAsInt(m_SCHID, clientIDList[i], CLIENT_INPUT_HARDWARE, &value[2]) != ERROR_ok)
				value[2] = -1;

			// missing information
			if(value[0] == -1 || value[1] == -1 || value[2] == -1)
				tmp->setIcon(0, m_clientIcon.normal);
			// double muted
			else if((value[0] || !value[2]) && value[1])
				tmp->setIcon(0, m_clientIcon.doubleMute);
			// micro muted
			else if(value[0] || !value[2])
				tmp->setIcon(0, m_clientIcon.microMute);
			// speaker muted
			else if(value[1])
				tmp->setIcon(0, m_clientIcon.speakerMute);
			// nothing muted
			else
				tmp->setIcon(0, m_clientIcon.normal);

			tmp->setData(1, Qt::UserRole, clientIDList[i]);
			tmp->setData(2, Qt::UserRole, client);
			m_clientList.push_back(tmp);

			it.entry->addChild(tmp);
			i++;
		}
		m_ts3.freeMemory(clientIDList);
	}

	m_tree->getTree()->adjustSize();
	m_tree->adjustSize();
}

void OverlayController::displayChannelList()
{
	m_tree->show();
}

void OverlayController::mute(bool value)
{
	m_muted = value;

	if (m_muted)
	{
		m_speakerOffset = 0;
		while (!m_speakers.isEmpty())
			delete m_speakers.takeFirst();
	}
}
