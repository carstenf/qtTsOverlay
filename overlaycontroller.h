#pragma once

#include "qttsoverlay_global.h"
#include "ts3_functions.h"
#include <QObject>
#include <QString>
#include <QWidget>
#include <QTimer>
#include <QVector>
#include <QLabel>
#include "ChannelListWidget.h"
#include <QIcon>


#define MAXLINES 5
#define BORDEROFFSET 5
#define SPACING 2
#define TIMEOUT 5000


struct ChannelInfo {
	uint64 id;
	QString name;
	uint64 parent;
	QTreeWidgetItem* entry;
	int order;
};


class QTTSOVERLAY_EXPORT OverlayController : public QObject
{
	Q_OBJECT

public:
	OverlayController(const struct TS3Functions funcs, quint64 serverConnectionHandlerID);
	~OverlayController();

private:
	const struct TS3Functions m_ts3;
	quint64 m_SCHID;
	bool m_muted;
	TreeState m_treeState;
	int m_screenWidth;
	int m_screenHeight;
	int m_speakerOffset;

	QIcon m_channelIcon;
	struct {
		QIcon normal;
		QIcon microMute;
		QIcon speakerMute;
		QIcon doubleMute;
	} m_clientIcon;
	QString m_stylesheet;

	QLabel* m_debugWindow;
	QVector<QWidget*> m_speakers;
	QVector<QWidget*> m_msgLines;
	ChannelListWidget* m_tree;
	QVector<ChannelInfo> m_channelList;
	QVector<QTreeWidgetItem*> m_clientList;

private:
	void deleteChatLine(QWidget* line, QTimer *timer = NULL);

private slots:
	void treeItemClicked(QTreeWidgetItem * item, int column);
	void changeTreeState(TreeState state);

public:
	void addChatLine(QString message);
	void addSpeaker(QString name);
	void removeSpeaker(QString name);
	void debugPrint(QString text);
	void reset();
	void updateChannelList();
	void updateClientList();
	void displayChannelList();
	void mute(bool value);
};
